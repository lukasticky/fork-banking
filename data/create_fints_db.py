import json
import openpyxl

data = {}
data['databases'] = []

BLZ = 1
BIC = 2
INSTITUTE = 3
ORGANIZATION = 6
URL = 24

wb = openpyxl.load_workbook("fints_institute.xlsx", data_only=True)
worksheet = wb["fints_institute_Master"]
for row in worksheet.iter_rows(min_row=2):
    if row[BLZ].value:
        data['databases'].append({
            'blz': row[BLZ].value,
            'bic': str(row[BIC].value).lower(),
            'institute': row[INSTITUTE].value,
            'organization': row[ORGANIZATION].value.lower() if row[ORGANIZATION].value else "",
            'url': row[URL].value
        })


with open('resources/database.json', 'w') as outfile:
    json.dump(data, outfile)
