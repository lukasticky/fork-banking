# Copyright (c) 2020-2021 Jan-Michael Brummer <jan.brummer@tabos.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from gettext import gettext as _
import locale
from gi.repository import Gtk, Adw, Gdk, Gio, GLib

from banking.widgets.account_row import AccountRow
from banking.widgets.transaction_row import TransactionRow
from banking.widgets.transaction_details import TransactionDetails # noqa
from banking.widgets.notification import Notification # noqa


@Gtk.Template(resource_path='/org/tabos/banking/ui/window.ui')
class Window(Adw.ApplicationWindow):
    __gtype_name__ = 'Window'

    _content_stack = Gtk.Template.Child()
    _setup_box = Gtk.Template.Child()
    _safe_password1 = Gtk.Template.Child()
    _safe_password2 = Gtk.Template.Child()
    _safe_label = Gtk.Template.Child()
    _save_safe_password = Gtk.Template.Child()
    _password_input_box = Gtk.Template.Child()
    _password_entry = Gtk.Template.Child()
    _password_button = Gtk.Template.Child()
    _account_listbox = Gtk.Template.Child()
    _transaction_listbox = Gtk.Template.Child()
    _transaction_details = Gtk.Template.Child()
    _total_balance_label = Gtk.Template.Child()
    _content_box = Gtk.Template.Child()
    _notification = Gtk.Template.Child()
    _sidebar = Gtk.Template.Child()
    _search_bar = Gtk.Template.Child()
    _search_entry = Gtk.Template.Child()
    start_assistant_button = Gtk.Template.Child()
    _content_window_title = Gtk.Template.Child()
    _detail_window = Gtk.Template.Child()
    _last_sync_label = Gtk.Template.Child()
    _lock_timer = None

    def __init__(self, backend, **kwargs):
        super().__init__(**kwargs)

        # Ensure welcome screen is the first one
        self._content_stack.set_visible_child_name('welcome')

        self._backend = backend
        self._backend.connect('accounts_refreshed',
                              self._on_accounts_refreshed)

        self._account_listbox.set_header_func(self._header_func)
        # self.account_listbox.set_sort_func(self._account_sort)

        self._transaction_listbox.set_sort_func(self._transaction_sort)
        self._transaction_listbox.set_filter_func(self._transaction_filter)
        self._transaction_listbox.add_css_class("content")
        self._transaction_listbox.connect(
            'row-activated',
            self._on_transaction_listbox_row_selected)
        self._transaction_rows = []

        self._account_store = Gio.ListStore()
        self._account_listbox.bind_model(
            self._account_store,
            self._create_widget_func)
        self._account_listbox.add_css_class("sidebar")

        # Connect search entry to search bar
        self._search_bar.connect_entry(self._search_entry)
        self._search_bar.set_key_capture_widget(self._detail_window)
        self._search_entry.connect("search-changed", self._on_search_changed)

        # Add global window key event controller
        controller = Gtk.EventControllerKey()
        controller.set_name("window-handler")
        controller.connect("key-pressed", self._on_key_pressed)
        self.add_controller(controller)

        self._transaction_details.back_button.connect(
            'clicked',
            self._on_details_back_button_clicked)

        self._password_input_box.add_css_class("linked")

        self._save_safe_password.set_sensitive(False)
        self._save_safe_password.add_css_class("suggested-action")
        self.start_assistant_button.add_css_class("suggested-action")

        if not self._backend.safe_password:
            self._content_stack.set_visible_child_name('setup')
        else:
            if self._backend._accounts:
                self._on_accounts_refreshed(None)
            # else:
            #    self.start_assistant_button.grab_focus()

            self._content_stack.set_visible_child_name('content')
            self.show_unlock_dialog()
            # self._content_box.set_visible_child(self._sidebar)

    def _create_widget_func(self, item):
        """
        Create a new widget
        :param item: item which is just forwarded as return.
        :return: item
        """
        return item

    def _on_key_pressed(self, controller, keyval, *_):
        """
        Window key handle which handles close window shortcuts and
        redirects everything else to the search bar.
        :param controller: event controller
        :param keyval: key value
        :param keycode: key code
        :param state: current key state
        :return True if key is handled in content stack, otherwise False
        """
        self._start_lock_timer()
        keyval_name = Gdk.keyval_name(keyval)
        if keyval_name in ('w', 'q'):
            self.destroy()

        if self._content_stack.get_visible_child_name() == 'content':
            return True

        return False

    def show_unlock_dialog(self):
        """
        In case safe password is set, switch to unlock dialog
        """
        if not self._backend.safe_password:
            return

        if self._content_stack.get_visible_child_name() == 'unlock':
            return

        self._password_entry.grab_focus()
        self._password_entry.set_text('')
        self._password_entry.set_visibility(False)
        self._password_entry.set_activates_default(True)
        self.set_default_widget(self._password_button)
        self._previous_content = self._content_stack.get_visible_child_name()
        self._content_stack.set_visible_child_name('unlock')

    @Gtk.Template.Callback()
    def _on_safe_password_entry_changed(self, _unused):
        password1 = self._safe_password1.get_text()

        self._save_safe_password.set_sensitive(False)
        if password1 == '':
            self._safe_label.set_text('')
            return

        if len(password1) < 4:
            self._safe_label.set_text(
                _('Password needs at least 4 characters.'))
            return

        if password1 == self._safe_password2.get_text():
            self._save_safe_password.set_sensitive(True)
            self._safe_label.set_text(_('Passwords match'))
        else:
            self._safe_label.set_text(_('Password does not match'))

    @Gtk.Template.Callback()
    def _on_safe_password_button_clicked(self, _):
        self._backend.safe_password = self._safe_password1.get_text()
        self._content_stack.set_visible_child_name('welcome')

    @Gtk.Template.Callback()
    def _on_password_entry_icon_release(self, _, position):
        """
        Switch visibility of password entry field
        :param widget: password entry widget
        :param position: icon position (PRIMARY or SECONDARY)
        """
        if position != Gtk.EntryIconPosition.SECONDARY:
            return

        self._password_entry.set_visibility(
            not self._password_entry.get_visibility())

    def _lock_timeout(self):
        """
        Lock timer timeout handling
         - Lock window
         - Send global notification
        """
        self._lock_timer = None
        self.show_unlock_dialog()
        self._notification.send_notification(
            _("Safe locked due to inactivity"))

    def _stop_lock_timer(self):
        """
        Stop lock timer
        """
        if self._lock_timer:
            GLib.source_remove(self._lock_timer)
            self._lock_timer = None

    def _start_lock_timer(self):
        """
        Start lock timer which locks the windows after timeout
        """
        self._stop_lock_timer()

        if self._content_stack.get_visible_child_name() != 'content':
            return

        timeout = 2 * 60
        self._lock_timer = GLib.timeout_add_seconds(timeout,
                                                    self._lock_timeout)

    @Gtk.Template.Callback()
    def _on_password_button_clicked(self, _):
        """
        Try to login with the password stored in password entry
        :param widget: password button widget
        """
        if not self._backend.is_safe_password_valid(
                self._password_entry.get_text()):
            return

        self._content_stack.set_visible_child_name(self._previous_content)
        if self._backend._accounts:
            self._backend.connect_client()
            self._start_lock_timer()
        else:
            self._content_stack.set_visible_child_name('welcome')

    # @Gtk.Template.Callback()
    def _on_transaction_listbox_row_selected(self, _, row):
        """
        Open transaction details dialog after clicking on a transaction row.
        """
        self._start_lock_timer()

        self._content_stack.set_visible_child(self._transaction_details)
        self._transaction_details.set_transaction(row)

    # @Gtk.Template.Callback()
    def _on_search_changed(self, _):
        self._transaction_listbox.invalidate_filter()

    @Gtk.Template.Callback()
    def _on_back_button_clicked(self, _):
        """
        Back button clicked means set active content box child to sidebar.
        :param button: button widget
        """
        self._start_lock_timer()
        self._content_box.set_visible_child(self._sidebar)

    def _on_details_back_button_clicked(self, button):
        self._start_lock_timer()
        self._content_stack.set_visible_child_name('content')

    def _account_sort(self, row1, row2):
        """
        Sort accounts based on iban information.
        :param row1: first row
        :param row2: second row
        :return: 0 if there is a match, 1 if less -1 if greater than row2
        """
        iban1 = row1.get_iban() or ''
        iban2 = row2.get_iban() or ''

        if iban1 < iban2:
            return -1
        if iban1 > iban2:
            return 1
        return 0

    def _transaction_sort(self, row1, row2):
        """
        Sort transactions based on date information
        :param row1: first row
        :param row2: second row
        :return: 0 if there is a match, 1 if less -1 if greater than row2
        """
        date1 = row1.get_date()
        date2 = row2.get_date()

        if date1 < date2:
            return 1
        if date1 > date2:
            return -1
        return 0

    def _transaction_filter(self, row):
        """
        Filter transactions (search for sub strings) based on
         - applicant name
         - posting text

         :param row: current row to filter
         :return: %True if there is a match otherwise %False
        """
        search_text = self._search_entry.get_text().lower()
        ret = search_text in row.get_applicant_name().lower()

        if not ret:
            ret = search_text in row.get_posting_text().lower()

        return ret

    def _header_func(self, row, before):
        """
        Add a separator line between rows
        :param row: current row
        :param before: row before current one
        """
        if not before:
            row.set_header(None)

        if row.get_header():
            return

        header = Gtk.Separator()
        header.show()
        row.set_header(header)

    @Gtk.Template.Callback()
    def _on_account_listbox_row_selected(self, _, row):
        """
        Fill transaction listbox with the correct
        account data and set visible window to detail window (right pane)
        :param box: account listbox
        :param row: activated row
        """
        self._start_lock_timer()

        # Clear existing entries
        for trans_row in self._transaction_rows:
            self._transaction_listbox.remove(trans_row)
        self._transaction_rows = []

        # Add new transactions
        self._content_window_title.set_title(row.account['product_name'])
        self._content_window_title.set_subtitle(row.account['iban'])

        if row.account['transactions']:
            for transaction in row.account['transactions']:
                row = TransactionRow(transaction)
                self._transaction_rows.append(row)
                self._transaction_listbox.append(row)

        self._content_box.set_visible_child(self._detail_window)

    def _on_accounts_refreshed(self, _button):
        """
        Accounts have been refresh, update window and it's children.
        """

        if self._lock_timer is None:
            self.show_unlock_dialog()
        else:
            self._content_stack.set_visible_child_name('content')

        total_balance = 0
        self._account_store.remove_all()

        for account in self._backend._accounts:
            row = AccountRow(account)
            self._account_store.append(row)

            total_balance = total_balance + float(account['balance'])

        if self._backend._accounts:
            currency = locale.currency(total_balance,
                                       grouping=True,
                                       symbol=False)
            self._total_balance_label.set_markup("<big>"
                                                 + currency
                                                 + ' '
                                                 + account['currency']
                                                 + "</big>")
            if float(total_balance) >= 0.0:
                self._total_balance_label.set_name('credit')

            self._last_sync_label.set_text(_('Last sync: ')
                                           + self._backend.last_sync)

    def _on_close_request(self, _):
        """
        On window close stop lock timer and allow close request
        :param data: unused
        """
        self._stop_lock_timer()
        return False
