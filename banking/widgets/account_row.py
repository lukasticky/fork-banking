# Copyright (c) 2020-2021 Jan-Michael Brummer <jan.brummer@tabos.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
Account row, showing bank account information

TODO: Move this into a glade ui file
"""

import locale
from gi.repository import GdkPixbuf, Gtk


class AccountRow(Gtk.ListBoxRow):
    def __init__(self, account):
        super().__init__()

        self.account = account

        grid = Gtk.Grid()
        grid.set_hexpand(False)
        grid.set_hexpand_set(True)
        grid.set_column_spacing(12)
        grid.set_margin_start(6)
        grid.set_margin_end(6)
        grid.set_margin_top(6)
        grid.set_margin_bottom(6)
        self.set_child(grid)

        if 'comdirect bank' in self.account['bank_name'].lower():
            pixbuf = GdkPixbuf.Pixbuf.new(
                GdkPixbuf.Colorspace.RGB, True, 8, 32, 32)
            pixbuf_original = GdkPixbuf.Pixbuf.new_from_resource_at_scale(
                "/org/tabos/banking/resources/comd.svg", 299, 111, True)
            pixbuf_original.scale(
                pixbuf, 0, 0, 32, 32, -30.0, -46.0, 1.0, 1.0,
                GdkPixbuf.InterpType.NEAREST)
        elif self.account['association'] == 'dsgv':
            pixbuf = GdkPixbuf.Pixbuf.new_from_resource_at_scale(
                "/org/tabos/banking/resources/dsgv.svg", 32, 32, True)
        elif self.account['association'] == 'bdb':
            pixbuf = GdkPixbuf.Pixbuf.new_from_resource_at_scale(
                "/org/tabos/banking/resources/bdb.jpg", 32, 32, True)
        elif self.account['association'] == 'bvr':
            pixbuf = GdkPixbuf.Pixbuf.new_from_resource_at_scale(
                "/org/tabos/banking/resources/bvr.svg", 32, 32, True)

        image = Gtk.Image()
        image.set_from_pixbuf(pixbuf)
        image.set_size_request(32, 32)
        grid.attach(image, 0, 0, 1, 3)

        product_name_label = Gtk.Label()
        product_name_label.set_markup('<b>'
                                      + self.account['product_name']
                                      + '</b>')
        product_name_label.set_wrap(True)
        product_name_label.set_halign(Gtk.Align.START)
        grid.attach(product_name_label, 1, 0, 1, 1)

        account_label = Gtk.Label()
        account_label.set_text(self.account['iban'] or '')
        account_label.set_wrap(True)
        account_label.set_halign(Gtk.Align.START)
        grid.attach(account_label, 1, 1, 1, 1)

        owner_name_label = Gtk.Label()
        owner_name_label.set_markup(
            '<small>' + self.account['owner_name'] + '</small>')
        owner_name_label.set_wrap(True)
        owner_name_label.set_halign(Gtk.Align.START)
        grid.attach(owner_name_label, 1, 2, 2, 1)

        balance = locale.currency(float(self.account['balance']),
                                  grouping=True,
                                  symbol=False)
        balance_label = Gtk.Label()
        balance_label.set_text(balance + ' ' + self.account['currency'])
        balance_label.set_hexpand(True)
        balance_label.set_halign(Gtk.Align.END)
        if float(self.account['balance']) < 0.0:
            balance_label.set_name('debit')
        grid.attach(balance_label, 2, 1, 1, 1)
