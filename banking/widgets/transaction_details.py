# Copyright (c) 2020-2021 Jan-Michael Brummer <jan.brummer@tabos.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
Transaction details
"""

import locale
from gi.repository import Gtk


@Gtk.Template(resource_path='/org/tabos/banking/ui/transaction_details.ui')
class TransactionDetails(Gtk.Box):
    __gtype_name__ = 'TransactionDetails'

    back_button = Gtk.Template.Child()
    _booking_date_label = Gtk.Template.Child()
    _value_label = Gtk.Template.Child()
    _amount_label = Gtk.Template.Child()
    _transaction_type_label = Gtk.Template.Child()

    _name_label = Gtk.Template.Child()
    _iban_label = Gtk.Template.Child()
    _bic_label = Gtk.Template.Child()

    _reference_label = Gtk.Template.Child()

    _creditor_id_label = Gtk.Template.Child()
    _mandate_reference_label = Gtk.Template.Child()
    _end_to_end_reference_label = Gtk.Template.Child()

    def set_transaction(self, transaction):
        self._booking_date_label.set_text(transaction.get_date())
        self._value_label.set_text(transaction.get_entry_date())
        amount = locale.currency(
            float(transaction.get_amount()),
            grouping=True, symbol=False)
        self._amount_label.set_text(amount)
        self._transaction_type_label.set_text(transaction.get_posting_text())

        self._name_label.set_text(transaction.get_applicant_name())
        self._iban_label.set_text(transaction.get_iban())
        self._bic_label.set_text(transaction.get_bic())

        self._reference_label.set_text(transaction.get_purpose())

        self._creditor_id_label.set_text(
            transaction.get_applicant_creditor_id())
        self._mandate_reference_label.set_text(
            transaction.get_additional_position_reference())
        self._end_to_end_reference_label.set_text(
            transaction.get_end_to_end_reference())
