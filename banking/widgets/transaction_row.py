# Copyright (c) 2020-2021 Jan-Michael Brummer <jan.brummer@tabos.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
Transaction row, showing transaction information
"""

import locale
import re
from gi.repository import Adw, Gtk

CATEGORY_DB = {
    "^ARAL AG": "fuel-symbolic",
    "^REWE ": "shopping-cart-symbolic",
    "^Netto Marken-Discoun": "shopping-cart-symbolic",
    "^DM Drogeriemarkt": "shopping-cart-symbolic",
    ".*MCDONALDS": "restaurant-symbolic",
    "^PayPal ": "money-symbolic",
    "Vodafone GmbH": "smartphone-symbolic",
    r"SCHAEFERS \d\d\d\d\d.*": "wheat-symbolic",
    r"SANDERS \d\d": "wheat-symbolic",
    ".*Apotheke.*": "plus-symbolic",
    "AMAZON.*": "shopping-cart-symbolic",
}


class TransactionRow(Adw.ActionRow):
    _applicant_name_label = Gtk.Template.Child()
    _sub_label = Gtk.Template.Child()
    _amount_label = Gtk.Template.Child()

    def __init__(self, transaction, **kwargs):
        super().__init__(**kwargs)

        self._transaction = transaction
        self.set_activatable(True)
        self.set_property('title-lines', 1)
        self.set_property('subtitle-lines', 1)

        if self._transaction['applicant_name']:
            name = self._transaction['applicant_name']
        else:
            name = self._transaction['posting_text']

        self.set_title(name)
        self.set_subtitle(self._transaction['date']
                          + ' - '
                          + (self._transaction['posting_text'] or ''))

        avatar = Adw.Avatar()

        for key in CATEGORY_DB:
            p = re.compile(key, re.IGNORECASE)
            match = p.match(name)
            if match:
                avatar.set_text(CATEGORY_DB[key])
                avatar.set_icon_name(CATEGORY_DB[key])
                avatar.set_size(32)
                break
        else:
            avatar.set_text(name)
            avatar.set_size(32)
            avatar.set_show_initials(True)

        self.add_prefix(avatar)

        amount = locale.currency(
            float(self._transaction['amount']['amount']),
            grouping=True, symbol=False)
        amount_label = Gtk.Label()
        amount_label.set_text(amount)

        self.add_suffix(amount_label)
        if float(self._transaction['amount']['amount']) >= 0.0:
            amount_label.set_name('credit')

    def _get_value(self, key):
        val = self._transaction.get(key)
        if val and val.strip():
            return val

        return ''

    def get_date(self):
        return self._get_value('date')

    def get_entry_date(self):
        return self._get_value('entry_date')

    def get_applicant_name(self):
        return self._get_value('applicant_name')

    def get_posting_text(self):
        return self._get_value('posting_text')

    def get_amount(self):
        return self._transaction['amount']['amount'] or ''

    def get_iban(self):
        return self._get_value('applicant_iban')

    def get_bic(self):
        return self._get_value('applicant_bin')

    def get_purpose(self):
        return self._get_value('purpose')

    def get_applicant_creditor_id(self):
        return self._get_value('applicant_creditor_id')

    def get_additional_position_reference(self):
        return self._get_value('additional_position_reference')

    def get_end_to_end_reference(self):
        return self._get_value('end_to_end_reference')
