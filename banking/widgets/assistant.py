# Copyright (c) 2020-2021 Jan-Michael Brummer <jan.brummer@tabos.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
Assistant window: Create account to access online banking information
based on fints standard.
"""

from gi.repository import Gtk


@Gtk.Template(resource_path='/org/tabos/banking/ui/assistant.ui')
class Assistant(Gtk.ApplicationWindow):
    """
    Create a new bank account configuration
    """
    __gtype_name__ = 'Assistant'

    _bank_entry = Gtk.Template.Child()
    _user_entry = Gtk.Template.Child()
    _password_entry = Gtk.Template.Child()
    _server_entry = Gtk.Template.Child()

    def __init__(self, backend, **kwargs):
        """
        Initialize assistant window
        """
        super().__init__(**kwargs)

        self._backend = backend
        self._user_entry.set_text(self._backend.user)
        self._bank_entry.set_text(self._backend.blz)
        self._server_entry.set_text(self._backend.server)
        self._password_entry.set_text(self._backend.password)
        self._institute = None

    @Gtk.Template.Callback()
    def _on_search_changed(self, _):
        """
        Try to lookup BIC or BLZ in our database and
        if found set url in server entry otherwise it will be empty
        :param entry: search entry widget
        """
        self._institute = self._backend.find_institute(
            self._bank_entry.get_text())

        if self._institute:
            self._server_entry.set_text(self._institute['url'])
        else:
            self._server_entry.set_text('')

    @Gtk.Template.Callback()
    def _on_done_clicked(self, _):
        """
        Store user input in backend and close assistant
        :param button: done button widget
        """
        self._backend.blz = self._institute['blz'] if self._institute else \
            self._bank_entry.get_text()
        self._backend.user = self._user_entry.get_text()
        self._backend.server = self._institute['url'] if self._institute else \
            self._server_entry.get_text()
        self._backend.password = self._password_entry.get_text()

        self.destroy()

        self._backend.refresh_accounts()
