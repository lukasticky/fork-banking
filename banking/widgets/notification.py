# Copyright (c) 2020-2021 Jan-Michael Brummer <jan.brummer@tabos.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
In-app notifications.
"""

from gi.repository import Adw, GLib, Gtk

REVEAL_TIME = 3.0


@Gtk.Template(resource_path="/org/tabos/banking/ui/notification.ui")
class Notification(Adw.Bin):

    __gtype_name__ = "Notification"

    event_id = None
    label = Gtk.Template.Child()
    revealer = Gtk.Template.Child()

    def send_notification(self, notification: str) -> None:
        self.label.set_label(notification)
        if self.event_id is not None:
            GLib.source_remove(self.event_id)
            self.event_id = None

        self.revealer.set_reveal_child(True)
        self.event_id = GLib.timeout_add_seconds(REVEAL_TIME,
                                                 self.__hide_notification)

    def __hide_notification(self) -> None:
        self.event_id = None
        self.revealer.set_reveal_child(False)
