# Copyright (c) 2020-2021 Jan-Michael Brummer <jan.brummer@tabos.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import datetime
import gi
import json
import logging
import os
import time
import hashlib
import mt940
from fints.client import FinTSOperations, FinTS3PinTanClient, NeedTANResponse
from fints.exceptions import FinTSClientPINError
from fints.formals import CreditDebit2
from fints.models import SEPAAccount
gi.require_version('Secret', '1')
from gi.repository import Gtk, Gio, GObject, GLib, Secret

import banking.fints_extra
from banking.widgets.mechanism_dialog import MechanismDialog
from banking.widgets.tan_dialog import TanDialog

BANKING_SCHEMA = Secret.Schema.new("org.tabos.banking.Store",
                                   Secret.SchemaFlags.NONE,
                                   {
                                       "user": Secret.SchemaAttributeType.STRING,
                                   }
                                   )

BASE_KEY = "org.tabos.banking"


class BankingBackend(GObject.Object):
    @GObject.Signal()
    def accounts_refreshed(self):
        self._load_accounts()

    def __init__(self):
        super().__init__()

        self._banking_path = os.path.join(GLib.get_user_data_dir(), "banking")
        if not os.path.exists(self._banking_path):
            os.makedirs(self._banking_path)

        logging.basicConfig(
            filename=os.path.join(self._banking_path, "banking.log"),
            level=logging.DEBUG)
        logging.info('Started')

        self._client = None
        self._settings = Gio.Settings.new(BASE_KEY)
        self._blz = self._settings.get_string('blz')
        self._user = self._settings.get_string('user')
        self._server = self._settings.get_string('server')
        self._password = ""
        self._safe_password = None
        self._mechanism = None
        self._mechanisms = None
        self._tan_ret = None
        self._mechanism_ret = None
        self._tan_msg = None
        self._last_sync = "-"
        self._datablob = os.path.join(self._banking_path, "fints.blob")

        gbytes = Gio.resources_lookup_data(
            '/org/tabos/banking/resources/database.json',
            Gio.ResourceLookupFlags.NONE)
        data = gbytes.get_data().decode('utf-8')
        self._client_db = json.loads(data)

        self._load_accounts()

    @property
    def user(self):
        return self._user

    @user.setter
    def user(self, user):
        self._settings.set_string('user', user)
        self._user = user

    @property
    def blz(self):
        return self._blz

    @blz.setter
    def blz(self, blz):
        self._settings.set_string('blz', str(blz))
        self._blz = str(blz)

    @property
    def server(self):
        return self._server

    @server.setter
    def server(self, server):
        self._settings.set_string('server', server)
        self._server = server

    @property
    def password(self):
        return self._password or ""

    @password.setter
    def password(self, password):
        Secret.password_store(BANKING_SCHEMA,
                              {"user": self._user},
                              Secret.COLLECTION_DEFAULT,
                              "Banking",
                              password,
                              None,
                              None)
        self._password = password

    @property
    def safe_password(self):
        return self._settings.get_string('safe-password-hash')

    @safe_password.setter
    def safe_password(self, safe_password):
        """
        Safe password is kept in memory and will be stored hashed in settings
        """
        str_hash = hashlib.md5(safe_password.encode('utf-8')).hexdigest()
        self._settings.set_string('safe-password-hash', str_hash)
        self._safe_password = safe_password

    def is_safe_password_valid(self, safe_password):
        """
        Check whether provided safe_password matches the one stored in our
        configuration.
        :param safe_password: user entered safe password to compare with
        :return: True on match otherwise False
        """
        safe_password_hash = self._settings.get_string('safe-password-hash')
        str_hash = hashlib.md5(safe_password.encode('utf-8')).hexdigest()
        if safe_password_hash == str_hash:
            self._safe_password = safe_password

            if self._accounts:
                self._password = Secret.password_lookup_sync(BANKING_SCHEMA,
                                                             {"user": self._user},
                                                             None)

            return True

        return False

    @property
    def accounts(self):
        return self._accounts

    @property
    def last_sync(self):
        return self._last_sync

    def find_institute(self, blz_or_bic):
        """
        Find institute based on user provided blz or bic information
        :param blz_or_bic: user provided blz or bic
        :return: database entry if found otherwise None
        """
        for db in self._client_db['databases']:
            blz = str(db['blz'])
            bic = str(db['bic'])
            if blz == blz_or_bic or bic.lower() == blz_or_bic.lower():
                return db

        return None

    def get_association(self):
        """
        Get association for the current user selected blz
        :return: organization name or None
        """
        for row in self._client_db['databases']:
            blz = str(row['blz'])
            if blz == self._blz:
                return row['organization']

        return None

    def _load_accounts(self):
        """
        Load accounts from disk.
        """
        self._accounts = []

        for root, dirs, _ in os.walk(self._banking_path):
            for name in dirs:
                file = os.path.join(root, name, 'settings.json')
                if not os.path.exists(file):
                    continue

                with open(file, "r") as infile:
                    account = json.load(infile)

                file = os.path.join(root, name, 'transactions.json')
                if not os.path.exists(file):
                    continue

                self._last_sync = str(time.ctime(os.path.getmtime(file)))
                transactions = None

                with open(file, "r") as infile:
                    try:
                        transactions = json.load(infile)
                    except ValueError:
                        transactions = "[]"

                account['transactions'] = transactions
                self._accounts.append(account)

    def _credentials_valid(self):
        """
        Check whether all necessary client data are set.
        :return: True if valid, otherwise False.
        """
        return \
            self._user != '' and \
            self._blz != '' and \
            self._server != '' and \
            self._password != ''

    def _on_tan_response(self, _, retval, mainloop):
        """
        Handling TAN responses
        :param dialog: tan dialog
        :param retval: user return value
        :param mainloop: main loop hack
        """
        self._tan_ret = retval
        mainloop.quit()

    def _ask_for_tan(self, response):
        """
        Ask user for a new TAN
        :param response: client response message
        """
        tan_dialog = TanDialog(response, use_header_bar=True)

        app = Gtk.Application.get_default()
        win = Gtk.Application.get_active_window(app)
        if win:
            tan_dialog.set_transient_for(win)

        # MainLoop is an ugly hack for the current API
        mainloop = GObject.MainLoop()
        tan_dialog.connect('response', self._on_tan_response, mainloop)
        tan_dialog.show()

        mainloop.run()

        if self._tan_ret == Gtk.ResponseType.APPLY:
            tan = tan_dialog.get_tan()
            response = self._client.send_tan(response, tan)
        else:
            response = None

        tan_dialog.destroy()

        return response

    def _on_mechanism_response(self, _, retval, mainloop):
        """
        Handling mechanisms responses
        :param dialog: tan dialog
        :param retval: user return value
        :param mainloop: main loop hack
        """
        self._mechanism_ret = retval
        mainloop.quit()

    def get_mechanism(self):
        """
        Ask user to select a TAN mechanism
        """
        self._client.fetch_tan_mechanisms()
        self._mechanisms = list(self._client.get_tan_mechanisms().items())
        app = Gtk.Application.get_default()
        win = Gtk.Application.get_active_window(app)

        dialog = MechanismDialog(self._mechanisms,
                                 use_header_bar=True)
        if win:
            dialog.set_modal(True)
            dialog.set_transient_for(win)

        # MainLoop is an ugly hack for the current API
        mainloop = GObject.MainLoop()
        dialog.connect('response', self._on_mechanism_response, mainloop)
        dialog.show()

        mainloop.run()

        if self._mechanism_ret == Gtk.ResponseType.APPLY:
            self._client.set_tan_mechanism(
                self._mechanisms[dialog.mechanism][0])

        dialog.destroy()

    def connect_client(self):
        """
        Connect to remote server using FinTS
        :return: True if connected, otherwise False
        """
        if self._client:
            return True

        if not self._credentials_valid():
            return False

        datablob = None
        try:
            file = open(self._datablob, "rb")
            datablob = file.read()
            logging.debug("datablob loaded")
        except IOError:
            logging.debug("No blob found")

        logging.debug('Connecting to server...')
        self._client = FinTS3PinTanClient(
            self._blz,
            self._user,
            self._password,
            self._server,
            from_data=datablob,
            product_id='AA3B821AAECEA62FB87C27EF3')

        self._mechanism = self._client.get_current_tan_mechanism()
        if self._mechanism is None:
            self.get_mechanism()

        if self._client.init_tan_response:
            self._ask_for_tan(self._client.init_tan_response)

        return True

    def disconnect_client(self):
        """
        Disconnect from remote server and store blob for further requests.
        """
        if not self._client:
            return

        datablob = self._client.deconstruct()
        if datablob:
            file = open(self._datablob, "wb")
            file.write(datablob)

    def _get_credit_card_balance(self, sepa_access, credit_card_number):
        credit_card_transactions = self._client.get_credit_card_transactions(
            sepa_access,
            credit_card_number,
            datetime.date.today() - datetime.timedelta(days=1),
            datetime.date.today())

        if not (credit_card_transactions
                and isinstance(
                    credit_card_transactions[0], banking.fints_extra.DIKKU2)):
            return None

        # Return fints.formals.Balance1 format
        balance1 = credit_card_transactions[0].balance
        return balance1

    def _get_credit_card_transactions(self, sepa_access, credit_card_number):
        credit_card_transactions = self._client.get_credit_card_transactions(
            sepa_access,
            credit_card_number,
            datetime.date.today() - datetime.timedelta(days=120),
            datetime.date.today())

        if not (credit_card_transactions
                and isinstance(
                    credit_card_transactions[0],
                    banking.fints_extra.DIKKU2)
                and hasattr(credit_card_transactions[0], 'transactions')):
            # Actually these are at least 3 different cases
            # If only the last condition is false, and there is
            # no 'transactions' field it only means that there were simply
            # no transactions in the give timeframe so retuning the empty list
            # is completely valid, but if we didn't get back
            # a banking.fints_extra.DIKKU2 type
            # then something very unexpected must have happened like a warning:
            # 'FinTSParserWarning: Ignoring parser error and
            #  returning generic object'
            # Or some completely unexpected type was returned and parsed in.
            return []

        # List of CreditCardTransaction1 objects
        transactions = credit_card_transactions[0].transactions

        # CreditCardTransaction1 type objects mapped to mt940 encodable dicts
        mt940_encodable_transactions = [{
            'iban': '',
            'date': str(t.receipt_date),
            'entry_date': str(t.booking_date),
            'guessed_entry_date': str(t.value_date),
            'currency': t.currency,
            'posting_text': '',
            'amount': {
                'amount': (
                    ('-' if t.credit_debit == CreditDebit2.DEBIT else '')
                    + str(t.booked_amount)),
                'currency': t.booked_currency
            },
            'applicant_name': '',
            'applicant_iban': '',
            'applicant_bin': '',
            'purpose': ' '.join([x for x in t.memo if x]),
            'applicant_creditor_id': '',
            'additional_position_reference': '',
            'end_to_end_reference': t.booking_reference

        } for t in transactions]

        return mt940_encodable_transactions

    def _load_data(self):
        """
        Load data from remote server
        """
        try:
            with self._client:
                info = self._client.get_information()
                sepa_accounts = self._client.get_sepa_accounts()

                if info:
                    logging.debug('Got generic bank information')
                    if 'accounts' in info:
                        logging.debug('Num accounts: %d',
                                      len(info['accounts']))

                for account in info['accounts']:
                    # Account info
                    account_info = {}
                    account_info['association'] = self.get_association()
                    account_info['bank_name'] = info['bank']['name']
                    account_info['product_name'] = account['product_name']
                    account_info['currency'] = account['currency']
                    # This is needed because window.py tries to sort by iban
                    # so it can't be None:
                    # TypeError: '<' not supported between instances of
                    # 'str' and 'NoneType'
                    account_info['iban'] = account.get('iban', '')
                    account_info['subaccount_number'] = (
                        account['subaccount_number'])
                    account_info['owner_name'] = ', '.join(
                        account['owner_name'])
                    account_info['last_updated'] = str(time.time())

                    logging.debug('Get balance for: %s',
                                  account_info['product_name'])

                    # Get balance
                    sepa_access = None
                    credit_card_number = None
                    for sepa_account in sepa_accounts:
                        if (sepa_account.iban == account['iban']
                                and account['supported_operations'][
                                    FinTSOperations.GET_TRANSACTIONS]):
                            sepa_access = sepa_account
                            logging.debug("SEPA access found")
                            break
                    if not sepa_access:
                        gcct_supported = account['supported_operations'][
                            FinTSOperations.GET_CREDIT_CARD_TRANSACTIONS]

                        if account['subaccount_number'] and gcct_supported:
                            # get_credit_card_transactions will need an
                            # 'accountnumber' field,
                            # but certain subaccounts only have an
                            # 'account_number' field, so let's wrap this
                            # into a SEPAAccount.
                            sepa_access = SEPAAccount(
                                iban='',
                                # The bic is a must have, so let's assume that
                                # there is at least 1 sepa_account
                                # Otherwise we should use db['bic']
                                bic=sepa_accounts[0].bic,
                                accountnumber=account['account_number'],
                                subaccount=account['subaccount_number'],
                                blz=self.blz)
                            credit_card_number = account['account_number']
                            logging.debug("Subaccount found")
                        else:
                            logging.debug('No SEPA access found, returning')
                            continue

                    if account['supported_operations'][
                            FinTSOperations.GET_BALANCE]:
                        balance = self._client.get_balance(sepa_access)
                        while isinstance(balance, NeedTANResponse):
                            balance = self._ask_for_tan(balance)
                        account_info['balance'] = str(balance.amount.amount)
                    elif account['supported_operations'][
                            FinTSOperations.GET_CREDIT_CARD_TRANSACTIONS]:
                        balance = self._get_credit_card_balance(
                            sepa_access, credit_card_number)
                        account_info['balance'] = str(balance.amount)
                    else:
                        break

                    if not balance:
                        continue

                    account_id = (account_info['iban']
                                  or account_info['subaccount_number'])
                    account_dir = os.path.join(GLib.get_user_data_dir(),
                                               'banking',
                                               account_id)
                    account_settings = os.path.join(account_dir,
                                                    'settings.json')

                    logging.debug('Storing generic settings to: %s',
                                  account_settings)
                    if not os.path.exists(account_dir):
                        os.makedirs(account_dir)

                    with open(account_settings, 'w') as outfile:
                        json.dump(account_info, outfile)

                    # Get transactions
                    logging.debug('Get transactions')
                    if (credit_card_number
                            and account['supported_operations'][
                                FinTSOperations.GET_CREDIT_CARD_TRANSACTIONS]):
                        transactions = self._get_credit_card_transactions(
                            sepa_access,
                            credit_card_number)
                    elif account['supported_operations'][
                            FinTSOperations.GET_TRANSACTIONS]:
                        transactions = self._client.get_transactions(
                            sepa_access,
                            datetime.date.today()
                            - datetime.timedelta(days=120),
                            datetime.date.today())
                    else:
                        break

                    while isinstance(transactions, NeedTANResponse):
                        transactions = self._ask_for_tan(transactions)

                    account_transactions = os.path.join(
                        account_dir,
                        'transactions.json')

                    if credit_card_number and transactions:
                        logging.debug('Storing creditcard transactions to: %s',
                                      account_transactions)
                        dump = json.dumps(transactions, indent=4)
                        with open(account_transactions, "w") as outfile:
                            outfile.write(dump)

                    elif transactions:
                        logging.debug('Storing transactions to: %s',
                                      account_transactions)
                        dump = json.dumps(
                            transactions,
                            indent=4,
                            cls=mt940.JSONEncoder)

                        with open(account_transactions, "w") as outfile:
                            outfile.write(dump)
                    elif not os.path.exists(account_transactions):
                        with open(account_transactions, "w") as outfile:
                            outfile.write("[]")
                    self._last_sync = str(time.ctime(os.path.getmtime(
                        account_transactions)))
        except FinTSClientPINError as err:
            app = Gtk.Application.get_default()
            win = Gtk.Application.get_active_window(app)
            dialog = Gtk.MessageDialog(
                transient_for=win,
                message_type=Gtk.MessageType.ERROR,
                buttons=Gtk.ButtonsType.CANCEL,
                text=err
            )
            dialog.set_modal(True)
            dialog.connect("response", self._on_error_dialog)
            dialog.show()

        self.emit('accounts_refreshed')

    def _on_error_dialog(self, window, _):
        """
        Destroy error window
        :param window: error window
        :param y: unused
        """
        Gtk.Window.destroy(window)

    def refresh_accounts(self):
        """
        Refresh account information.
        """
        if not self._client:
            ret = self.connect_client()
            if not ret:
                return

        GLib.idle_add(self._load_data)
