# Copyright (c) 2020-2021 Jan-Michael Brummer <jan.brummer@tabos.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
Initialize banking application.
"""

from gi.repository import Gdk, Gtk, Gio

from banking.widgets.about_dialog import AboutDialog
from banking.widgets.assistant import Assistant
from banking.window import Window
from banking.backend import BankingBackend


class Application(Gtk.Application):
    def __init__(self, application_id):
        """
        Initialize main:
          - Check stored online account credentials
          - add app actions
        """
        super().__init__(application_id=application_id,
                         flags=Gio.ApplicationFlags.FLAGS_NONE)

        self._init_style()
        self._backend = BankingBackend()
        self._window = None
        self._assistant = None

        # Add assistant action
        action = Gio.SimpleAction.new("assistant", None)
        action.connect("activate", self._on_assistant)
        self.add_action(action)

        # Add refresh action
        action = Gio.SimpleAction.new("refresh", None)
        action.connect("activate", self._on_refresh)
        self.add_action(action)

        # Add lock action
        action = Gio.SimpleAction.new("lock", None)
        action.connect("activate", self._on_lock)
        self.add_action(action)

        # Add about action
        action = Gio.SimpleAction.new("about", None)
        action.connect("activate", self._on_about)
        self.add_action(action)

        # Register application and dbus signal for screensaver
        self.register()
        self._register_dbus_signal()

    def _init_style(self):
        style_provider = Gtk.CssProvider()
        style_provider.load_from_resource(
            '/org/tabos/banking/org.tabos.banking.css')
        Gtk.StyleContext.add_provider_for_display(
            Gdk.Display.get_default(),
            style_provider,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)

    def _on_session_lock(self,
                         _connection,
                         _unique_name,
                         _object_path,
                         _interface,
                         _signal,
                         state):
        if state[0]:
            self._window.show_unlock_dialog()

    def _register_dbus_signal(self) -> None:
        """
        Register a listener so we get notified about screensaver kicking in
        """
        connection = Gio.Application.get_default().get_dbus_connection()
        self.dbus_subscription_id = connection.signal_subscribe(
            None, "org.gnome.ScreenSaver", "ActiveChanged",
            "/org/gnome/ScreenSaver", None,
            Gio.DBusSignalFlags.NONE, self._on_session_lock,)

    def open_assistant(self):
        """
        Open assistant window and connect to done button.
        """
        self._assistant = Assistant(
            application=self,
            backend=self._backend)

        self._assistant.set_transient_for(self._window)
        self._assistant.show()

    def _start_assistant(self, _):
        self.open_assistant()

    def _on_assistant(self, *_):
        """
        Open assistant
        :param action: simple action
        :param value: unused value
        """
        self.open_assistant()

    def _on_refresh(self, *_):
        """
        Refresh data
        :param action: simple action
        :param value: unused value
        """
        self._backend.refresh_accounts()

    def _on_lock(self, *_):
        """
        Lock window
        :param action: simple action
        :param value: unused value
        """
        self._window.show_unlock_dialog()

    def _on_about(self, *_):
        """
        Open about dialog
        :param action: simple action
        :param value: unused value
        """
        about = AboutDialog()
        about.props.transient_for = self._window
        about.present()

    def do_activate(self):  # pylint: disable=arguments-differ
        """
        Activation:
        Open the main window.
        """
        if not self._window:
            self._window = Window(self._backend, application=self)
            self._window.start_assistant_button.connect('clicked',
                                                        self._start_assistant)

        self._window.present()

    def do_shutdown(self):  # pylint: disable=arguments-differ
        """
        Shutdown application:
         - Shutdown backend
        """
        self._backend.disconnect_client()
        Gtk.Application.do_shutdown(self)
